package com.study.cms.org.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Emp {

    private Long id;
    private String username;
    private String password;
    private String email;
    private String headImage;
    private Integer age;
    private Dept dept;

}
