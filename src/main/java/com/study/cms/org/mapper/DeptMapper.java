package com.study.cms.org.mapper;

import com.study.cms.basic.mapper.BaseMapper;

import com.study.cms.org.domain.Dept;


import java.io.Serializable;
import java.util.List;

public interface DeptMapper  extends BaseMapper<Dept> {
//    List<Dept> selectAll();
//    Dept selectByid(Serializable id);
//
//    void insert(Dept dept);
//    void update(Dept dept);
//    void delete(Serializable id);
//
//    long loadTotal(PageList query);
//    List<Dept> loadData(PageList query);
//
//    void patchremove(List<Long> list);

    List<Dept> gettree();
}
