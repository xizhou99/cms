package com.study.cms.org.mapper;


import com.study.cms.auth.Dto.EmpDTO;
import com.study.cms.auth.domain.Role;
import com.study.cms.basic.mapper.BaseMapper;
import com.study.cms.org.domain.Emp;


import java.io.Serializable;
import java.util.List;

public interface EmpMapper extends BaseMapper<Emp> {



    Emp loadByUsername(String username);

    List<Role> gettree();

    List<Long> getroleid();

    List<Long> getrolebyid(Long id);

    void setemprole(EmpDTO dto);

    void deleteroleByempid(Long empid);
}
