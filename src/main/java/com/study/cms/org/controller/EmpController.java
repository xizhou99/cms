package com.study.cms.org.controller;


import com.study.cms.auth.Dto.EmpDTO;
import com.study.cms.auth.Dto.MenuDTO;
import com.study.cms.auth.annotation.RonghuanetPermission;
import com.study.cms.auth.domain.Permission;
import com.study.cms.auth.domain.Role;

import com.study.cms.basic.query.BaseQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.org.domain.Dept;
import com.study.cms.org.domain.Emp;
import com.study.cms.org.service.EmpService;
import com.study.cms.org.utils.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/emp")
@Api(value = "员工的API",description="员工相关的CRUD功能")
@RonghuanetPermission(name = "员工")
public class EmpController {
    @Autowired
    private EmpService empService;

    /**
     * @param emp
     * @return
     */
    @PutMapping
    @ApiOperation(value = "新增/修改")
    @RonghuanetPermission(name = "新增/修改")
    public AjaxResult addOrUpdate(@RequestBody Emp emp) {
        try {
            if (Objects.isNull(emp.getId())) {
                System.out.println("---新增---");
                System.out.println(emp);
                empService.insert(emp);
                //@TODO 调用empService做新增操作
            } else {
                //@TODO 调用empService做修改操作
                System.out.println("---修改---");
                System.out.println(emp);
                empService.update(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败!");
        }
        return AjaxResult.me();
    }

    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    @RonghuanetPermission(name = "删除")
    public AjaxResult delById(@PathVariable("id") Long id) {
        try {
            empService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!");
        }
        return AjaxResult.me();
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "通过ID查询")
    @RonghuanetPermission(name = "通过ID查询")
    public AjaxResult getById(@PathVariable("id") Long id) {
        try {
            // @TODO 调用departmentService的查询方法
            Emp emp = empService.selectByid(id);
            return AjaxResult.me().setData(emp);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }

    /**
     * 查询所有
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "查询所有")
    @RonghuanetPermission(name = "查询所有")
    public AjaxResult getAll() {
        try {
            List<Emp> emps = empService.selectAll();
            return AjaxResult.me().setData(emps);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }

    @PostMapping
    @ApiOperation(value = "员工分页查询")
    @RonghuanetPermission(name = "员工分页查询")
    public AjaxResult PageList(@RequestBody BaseQuery pageListQuery) {
        System.out.println(pageListQuery);
        try {
            PageList<Emp> deptPageList = empService.pageList(pageListQuery);
            return AjaxResult.me().setData(deptPageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

    @PatchMapping
    @ApiOperation(value = "批量删除")
    @RonghuanetPermission(name = "批量删除")
    public AjaxResult patchRemove(@RequestBody List<Long> ids) {
        System.out.println(ids);
        try {
            empService.patchremove(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败");
        }
    }


    @GetMapping("/tree")
    @RonghuanetPermission(name = "角色树")
    public AjaxResult tree(){
        try {
            List<Role> list =empService.gettree();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("角色树查询失败!"+e.getMessage());
        }
    }

    @GetMapping("/ids")
    public AjaxResult getRoleid(){
        try {
            List<Long> ids = empService.getroleid();
            return AjaxResult.me().setData(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置角色失败!"+e.getMessage());
        }
    }

    @GetMapping("/getroleemp/{id}")
    public AjaxResult getrolebyid(@PathVariable("id")Long id){
        try {
            List<Long> ids = empService.getrolebyid(id);
            return AjaxResult.me().setData(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }

    @PostMapping("/saverole")
    public AjaxResult setEmprole(@RequestBody EmpDTO dto){
        try {
            empService.setemprole(dto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }


}
