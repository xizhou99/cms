package com.study.cms.org.controller;


import com.study.cms.auth.annotation.RonghuanetPermission;

import com.study.cms.basic.query.BaseQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.org.domain.Dept;
import com.study.cms.org.service.DeptService;
import com.study.cms.org.utils.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/dept")
@Api(value = "部门的API",description="部门相关的CRUD功能")
@RonghuanetPermission(name = "部门")
public class DeptController {
    @Autowired
    private DeptService deptService;


    /**
     * 新增或修改的保存操作
     * @param dept
     * @return
     */
    /**
     * 表明接口的请求方式
     * 方式一:@RequestMapping("/xx/xx")
     * @RequestMapping(method = RequestMethod.PUT) // 表明当前是一个PUT请求方式
     * 方式二:@PutMapping
     * 对于前后端分离来说,增删改操作要告知前端一下,当前这个操作是否成功,如果操作不成功,要返回不成功的原因是什么
     * 所以增删改操作,要返回给前端两个数据  success:true/false 来表明操作是成功还是失败  message:返回的信息(特别是失败,原因是一定要返回的)
     * 怎么返回?
     * 方式一: 字符串拼接 return "{\"success\":false,\"message\":\"\"}"; 拼接起来麻烦,而且很容易出错
     * 方式二: Map 通过Map对象返回  在返回时框架底层会帮我们自动将Map对象转成JSON格式的字符串返回  但是写起来也比较麻烦
     * 方式三: 自定义一个返回值对象 AjaxResult  框架底层也会自动帮我们把对象转成JSON格式的字符串返回
     *
     * PUT/POST请求 在接收对象作为参数时,要用@RequestBody
     * @param dept
     */
    @PutMapping
    @RonghuanetPermission(name = "新增")
    @ApiOperation(value = "新增" )
    public AjaxResult addOrUpdate(@RequestBody Dept dept){
        System.out.println("=========");
        System.out.println(dept);
        try {
            if(Objects.isNull(dept.getId())){
                //@TODO 调用departmentService做新增操作
                deptService.insert(dept);
            }else{
                //@TODO 调用departmentService做修改操作
                deptService.update(dept);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败!");
        }
        return AjaxResult.me();
    }

    /**
     * 根据ID删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除" )
    @RonghuanetPermission(name = "删除")
    public AjaxResult delById(@PathVariable("id")Long id){
        try {
            deptService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!");
        }
        return AjaxResult.me();
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "通过ID查询" )
    @RonghuanetPermission(name = "通过ID查询")
    public AjaxResult getById(@PathVariable("id")Long id){
        try {
            // @TODO 调用departmentService的查询方法
            deptService.selectByid(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
        return AjaxResult.me();
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping
    @ApiOperation(value = "查询所有" )
    @RonghuanetPermission(name = "查询所有")
    public AjaxResult getAll(){
        try {
            List<Dept> departments = deptService.selectAll();
            return AjaxResult.me().setData(departments);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }


    @PostMapping
    @ApiOperation(value = "分页查询")
    @RonghuanetPermission(name = "分页查询")
    public AjaxResult PageList(@RequestBody BaseQuery pageListQuery){
        System.out.println(pageListQuery);
        try {
            PageList<Dept> deptPageList = deptService.pageList(pageListQuery);
            return AjaxResult.me().setData(deptPageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

    @PatchMapping
    @ApiOperation(value = "批量删除")
    @RonghuanetPermission(name = "批量删除")
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        System.out.println(ids);
        try {
            deptService.patchremove(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败");
        }
    }

    @GetMapping("/tree")
    @RonghuanetPermission(name = "部门树")
    public AjaxResult tree(){
        try {
            List<Dept> list =deptService.gettree();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("部门树查询失败!"+e.getMessage());
        }
    }




}
