package com.study.cms.org.service;


import com.study.cms.basic.service.BaseService;
import com.study.cms.org.domain.Dept;

import java.util.List;

public interface DeptService extends BaseService<Dept> {
//    List<Dept> selectAll();
//    Dept selectByid(Serializable id);
//
//    void insert(Dept dept);
//    void update(Dept dept);
//    void delete(Serializable id);
//
//    PageList<Dept> pageList(PageList query);
//
//    void patchremove(List<Long> list);

    List<Dept> gettree();
}
