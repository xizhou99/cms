package com.study.cms.org.service.impl;

import com.study.cms.basic.service.impl.BaseServiceImpl;
import com.study.cms.org.domain.Dept;
import com.study.cms.org.mapper.DeptMapper;
import com.study.cms.org.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Service
public class DeptServiceImpl extends BaseServiceImpl<Dept> implements DeptService {

    @Autowired
    private DeptMapper deptMapper;




    @Transactional
    @Override
    public void insert(Dept dept) {
        System.out.println("新增---------");
        System.out.println(dept);
        dept.setCreate_time(new Date());
        deptMapper.insert(dept);

        String path="";
        if(Objects.nonNull(dept.getDept())){
            // 根据父部门的id，查询父部门的信息
            Dept parent = deptMapper.selectByid(dept.getDept().getId());
            // 父部门存在，该部门path = 父部门的path+"/"+自己的id
            path = parent.getPath()+"/"+dept.getId();
        }else{
            // 父部门不存在，那么该部门的path=/自己的id
            path = "/"+dept.getId();
        }
        dept.setPath(path);
        deptMapper.update(dept);

    }

    @Transactional
    @Override
    public void update(Dept dept) {
        System.out.println("修改-----------");
        System.out.println(dept);
        dept.setUpdate_time(new Date());

        String path="";

//        Dept dept1 = deptMapper.selectByid(dept.getDept().getId());
        if(Objects.nonNull(dept.getDept().getId())){
            // 根据父部门的id，查询父部门的信息
            Dept parent = deptMapper.selectByid(dept.getDept().getId());
            // 父部门存在，该部门path = 父部门的path+"/"+自己的id
            path = parent.getPath()+"/"+dept.getId();
        }else{
            // 父部门不存在，那么该部门的path=/自己的id
            path = "/"+dept.getId();
        }
        dept.setPath(path);

        deptMapper.update(dept);
    }





    @Override
    public List<Dept> gettree() {
        return deptMapper.gettree();
    }
}
