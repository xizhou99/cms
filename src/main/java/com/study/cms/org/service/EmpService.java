package com.study.cms.org.service;

import com.study.cms.auth.Dto.EmpDTO;
import com.study.cms.auth.domain.Role;
import com.study.cms.basic.service.BaseService;
import com.study.cms.org.domain.Emp;

import java.util.List;

public interface EmpService extends BaseService<Emp> {
//    Emp selectByid(Serializable id);
//    List<Emp> selectAll();
//    void insert(Emp emp);
//    void update(Emp emp);
//    void delete(Serializable id);
//
//    PageList<Emp> pageList(PageList pageListQuery);
//
//    void patchremove(List<Long> ids);

    List<Role> gettree();

    List<Long> getroleid();

    List<Long> getrolebyid(Long id);

    void setemprole(EmpDTO dto);
}
