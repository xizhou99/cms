package com.study.cms.org.service.impl;

import com.study.cms.auth.Dto.EmpDTO;
import com.study.cms.auth.domain.Role;

import com.study.cms.basic.service.impl.BaseServiceImpl;
import com.study.cms.org.domain.Dept;
import com.study.cms.org.domain.Emp;
import com.study.cms.org.mapper.EmpMapper;
import com.study.cms.org.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Service
public class EmpServiceImpl extends BaseServiceImpl<Emp> implements EmpService {

    @Autowired
    private EmpMapper empMapper;




    @Override
    public List<Role> gettree() {
        return empMapper.gettree();
    }

    @Override
    public List<Long> getroleid() {
        return empMapper.getroleid();
    }

    @Override
    public List<Long> getrolebyid(Long id) {
        return empMapper.getrolebyid(id);
    }

    @Override
    public void setemprole(EmpDTO dto) {
        empMapper.deleteroleByempid(dto.getEmpid());
        empMapper.setemprole(dto);
    }
}
