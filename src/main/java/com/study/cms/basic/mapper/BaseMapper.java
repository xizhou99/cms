package com.study.cms.basic.mapper;


import com.study.cms.basic.query.BaseQuery;
import com.study.cms.basic.utils.PageList;

import java.io.Serializable;
import java.util.List;

public interface BaseMapper<T> {
    List<T> selectAll();

    T selectByid(Serializable id);

    void delete(Serializable id);

    void insert(T t);

    void update(T t);

    long loadTotal(BaseQuery pageListQuery);

    List<T> loadData(BaseQuery pageListQuery);

    void removeall(List<Long> ids);
}
