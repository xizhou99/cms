package com.study.cms.basic.utils;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageList<T> {
    /**
     * 总条数
     */
    private Long total = 0L;

    /**
     * 当前页数据
     */
    private List<T> rows = new ArrayList<>();
}
