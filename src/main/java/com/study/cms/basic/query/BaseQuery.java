package com.study.cms.basic.query;

import lombok.Data;

@Data
public class BaseQuery {

    private Integer currentPage;
    private Integer pageSize;

    private String keyword;


    public Integer getStart(){

        return  (currentPage-1)*pageSize;

    }

}
