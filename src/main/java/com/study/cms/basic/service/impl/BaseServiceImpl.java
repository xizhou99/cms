package com.study.cms.basic.service.impl;

import com.study.cms.auth.domain.Menu;
import com.study.cms.basic.mapper.BaseMapper;
import com.study.cms.basic.query.BaseQuery;
import com.study.cms.basic.service.BaseService;
import com.study.cms.basic.utils.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements BaseService<T> {

    @Autowired
    private BaseMapper<T> baseMapper;


    @Override
    public List selectAll() {
        return baseMapper.selectAll();
    }

    @Override
    public T selectByid(Serializable id) {
        return baseMapper.selectByid(id);
    }

    @Override
    @Transactional
    public void delete(Serializable id) {
            baseMapper.delete(id);
    }

    @Override
    @Transactional
    public void insert(T o) {
            baseMapper.insert(o);
    }

    @Override
    @Transactional
    public void update(T o) {
            baseMapper.update(o);
    }

    @Override
    public PageList<T> pageList(BaseQuery pageListQuery) {
        long total = baseMapper.loadTotal(pageListQuery);
        if(total > 0){
            List<T> rows = baseMapper.loadData(pageListQuery);
            return new PageList<>(total,rows);
        }
        return new PageList<>();
    }

    @Override
    @Transactional
    public void patchremove(List ids) {
            baseMapper.removeall(ids);
    }
}
