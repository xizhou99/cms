package com.study.cms.basic.service;


import com.study.cms.basic.query.BaseQuery;
import com.study.cms.basic.utils.PageList;


import java.io.Serializable;
import java.util.List;

public interface BaseService<T> {
    List<T> selectAll();

    T selectByid(Serializable id);

    void delete(Serializable id);

    void insert(T t);

    void update(T t);

    PageList<T> pageList(BaseQuery baseQuery);

    void patchremove(List<Long> ids);
}
