package com.study.cms.auth.service;


import com.study.cms.auth.domain.Permission;

import com.study.cms.basic.service.BaseService;

import java.util.List;


public interface PermissionService extends BaseService<Permission> {


    List<Permission> gettree();

    List<String> getRolePermissionSns();

    List<String> getSnsbyid(Long id);
}
