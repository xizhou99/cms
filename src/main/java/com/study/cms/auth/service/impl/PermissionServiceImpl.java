package com.study.cms.auth.service.impl;

import com.study.cms.auth.domain.Permission;
import com.study.cms.auth.mapper.PermissionMapper;

import com.study.cms.auth.service.PermissionService;
import com.study.cms.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PermissionServiceImpl extends BaseServiceImpl<Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;


    @Override
    public List<Permission> gettree() {
        return permissionMapper.gettree();
    }

    @Override
    public List<String> getRolePermissionSns() {
        return permissionMapper.getPermissionSns();
    }

    @Override
    public List<String> getSnsbyid(Long id) {
        return permissionMapper.getSnsbyid(id);
    }
}
