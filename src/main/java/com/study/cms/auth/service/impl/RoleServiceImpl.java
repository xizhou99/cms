package com.study.cms.auth.service.impl;

import com.study.cms.auth.Dto.MenuDTO;
import com.study.cms.auth.Dto.RolePermissionDTO;
import com.study.cms.auth.domain.Menu;
import com.study.cms.auth.domain.Role;
import com.study.cms.auth.mapper.RoleMapper;

import com.study.cms.auth.service.RoleService;
import com.study.cms.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true,propagation= Propagation.SUPPORTS)
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;


    @Override
    @Transactional
    public void setRolePermisison(RolePermissionDTO dto) {
        // 将角色ID和对应的权限SN保存到t_auth_role_permission
        // 思考? 如果这个角色之前已经存在权限了,怎么办?  先删除该角色对应的权限,然后再来新增
        // 1 根据角色ID,删除t_auth_role_permission的数据
        roleMapper.deleteRolePermissionByRoleId(dto.getRoleId());

        // 2 保存角色ID和对应的权限SN保存到t_auth_role_permission
        // 方式一:循环dto.permissionSns,每次循环都保存roleId和权限的sn到数据库
        // 方式二: 批量新增 - 操作一次数据库
        roleMapper.saveRolePermission(dto);
    }

    @Override
    public List<Menu> gettree() {
        return roleMapper.gettree();
    }

    @Override
    public List<Long> getmenuid() {
        return roleMapper.getmenuid();
    }

    @Override
    public void setRoleMenu(MenuDTO dto) {
        roleMapper.deleteMenuByRoleId(dto.getRoleId());
        roleMapper.saveMenu(dto);
    }
}
