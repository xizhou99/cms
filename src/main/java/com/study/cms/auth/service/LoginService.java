package com.study.cms.auth.service;

import com.study.cms.auth.Dto.LoginDTO;

import java.util.Map;

public interface LoginService {

    Map<String, Object> login(LoginDTO dto);
}
