package com.study.cms.auth.service;

import com.study.cms.basic.service.BaseService;

import java.util.List;

public interface MenuService extends BaseService<com.study.cms.auth.domain.Menu> {

//    List<MenuQuery> selectAll();
//
//    MenuQuery selectByid(Long id);
//
//    void delete(Long id);
//
//    void insert(MenuQuery menu);
//
//    void update(MenuQuery menu);
//
//    PageList<MenuQuery> pageList(MenuQuery menuQuery);
//
//    void patchremove(List<Long> ids);


    List<Long> geymenuid(Long id);

    List<com.study.cms.auth.domain.Menu> gettree(Long id);
}
