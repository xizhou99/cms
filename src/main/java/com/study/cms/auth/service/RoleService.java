package com.study.cms.auth.service;

import com.study.cms.auth.Dto.MenuDTO;
import com.study.cms.auth.Dto.RolePermissionDTO;
import com.study.cms.auth.domain.Menu;
import com.study.cms.basic.service.BaseService;

import java.util.List;

public interface RoleService extends BaseService<com.study.cms.auth.domain.Role> {
//    List<RoleQuery> selectAll();
//    RoleQuery selectByid(Serializable id);
//
//    void insert(RoleQuery role);
//    void update(RoleQuery role);
//    void delete(Serializable id);
//
//    PageList<RoleQuery> pageList(RoleQuery query);
//
//    void patchremove(List<Long> list);

    void setRolePermisison(RolePermissionDTO dto);

    List<Menu> gettree();

    List<Long> getmenuid();

    void setRoleMenu(MenuDTO dto);
}
