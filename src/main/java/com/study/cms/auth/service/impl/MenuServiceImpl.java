package com.study.cms.auth.service.impl;

import com.study.cms.auth.mapper.MenuMapper;
import com.study.cms.auth.service.MenuService;
import com.study.cms.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl extends BaseServiceImpl<com.study.cms.auth.domain.Menu> implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

//    @Override
//    public List<MenuQuery> selectAll() {
//        return menuMapper.selectAll();
//    }
//
//    @Override
//    public MenuQuery selectByid(Long id) {
//        return menuMapper.selectByid(id);
//
//    }
//
//    @Override
//    public void delete(Long id) {
//        menuMapper.delete(id);
//    }
//
//    @Override
//    public void insert(MenuQuery menu) {
//        menuMapper.insert(menu);
//    }
//
//    @Override
//    public void update(MenuQuery menu) {
//        menuMapper.update(menu);
//    }
//
//    @Override
//    public PageList<MenuQuery> pageList(MenuQuery menuQuery) {
//        long total = menuMapper.loadTotal(menuQuery);
//        if(total > 0){
//            List<MenuQuery> rows = menuMapper.loadData(menuQuery);
//            return new PageList<>(total,rows);
//        }
//        return new PageList<>();
//    }
//
//    @Override
//    public void patchremove(List<Long> ids) {
//        menuMapper.removeall(ids);
//    }

    @Override
    public List<Long> geymenuid(Long id) {
        return menuMapper.getmenuid(id);
    }

    @Override
    public List<com.study.cms.auth.domain.Menu> gettree(Long id) {
        return menuMapper.gettree(id);
    }


}
