package com.study.cms.auth.controller;


import com.study.cms.auth.Dto.LoginDTO;
import com.study.cms.auth.annotation.RonghuanetPermission;
import com.study.cms.auth.service.LoginService;
import com.study.cms.org.utils.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController

public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginDTO dto){
        try {
            Map<String,Object> map = loginService.login(dto);
            return AjaxResult.me().setData(map);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("登录失败!"+e.getMessage());
        }
    }

    @PostMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        try {
            // 获取请求头中传递的token
            String token = request.getHeader("token");

            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("登出失败!"+e.getMessage());
        }
    }
}
