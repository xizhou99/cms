package com.study.cms.auth.controller;

import com.study.cms.auth.annotation.RonghuanetPermission;
import com.study.cms.auth.domain.Permission;


import com.study.cms.auth.service.PermissionService;
import com.study.cms.basic.query.PermissonQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.org.utils.AjaxResult;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RonghuanetPermission(name = "权限")
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    public PermissionService permissionService;

    @PostMapping
    @ApiOperation(value = "分页查询")
    @RonghuanetPermission(name = "分页查询")
    public AjaxResult PageList(@RequestBody PermissonQuery roleQuery){
        System.out.println(roleQuery);
        try {
            PageList<Permission> rolePageList = permissionService.pageList(roleQuery);
            return AjaxResult.me().setData(rolePageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

    @GetMapping("/tree")
    @RonghuanetPermission(name = "部门树")
    public AjaxResult tree(){
        try {
            List<Permission> list =permissionService.gettree();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("权限树查询失败!"+e.getMessage());
        }
    }


    @GetMapping("/permissionSns/sns")
    @RonghuanetPermission(name = "获取所有sns标识")
    public AjaxResult getRolePermissionSns(){
        try {
            List<String> sns = permissionService.getRolePermissionSns();
            return AjaxResult.me().setData(sns);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @RonghuanetPermission(name = "通过角色id查找sn标识")
    public AjaxResult getSnsbyid(@PathVariable("id")Long id){
        try {
            List<String> sns = permissionService.getSnsbyid(id);
            return AjaxResult.me().setData(sns);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }


}
