package com.study.cms.auth.controller;


import com.study.cms.auth.annotation.RonghuanetPermission;

import com.study.cms.auth.domain.Menu;
import com.study.cms.auth.service.MenuService;
import com.study.cms.basic.query.MenuQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.org.utils.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/menu")
@RonghuanetPermission(name = "菜单管理")
public class MenuController {

    @Autowired
    private MenuService menuService;


    //  新增/修改
    @PutMapping
    @RonghuanetPermission(name = "新增/修改")
    public AjaxResult addorupdate(@RequestBody com.study.cms.auth.domain.Menu menu){
        try {
            if(Objects.isNull(menu.getId())){
                //@TODO 调用departmentService做新增操作
                System.out.println("------新增-----");
                System.out.println(menu);
                menuService.insert(menu);
            }else{
                //@TODO 调用departmentService做修改操作
                System.out.println("------修改-----");
                System.out.println(menu);
                menuService.update(menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败!");
        }
        return AjaxResult.me();
    }


    @DeleteMapping("/{id}")
    @RonghuanetPermission(name = "删除")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            menuService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!");
        }
        return AjaxResult.me();
    }

    @GetMapping("/{id}")
    @RonghuanetPermission(name = "通过id查询")
    public AjaxResult getbyid(@PathVariable("id")Long id){
        try {
           com.study.cms.auth.domain.Menu menu =  menuService.selectByid(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
        return AjaxResult.me();
    }


    @GetMapping
    @ApiOperation(value = "查询所有" )
    @RonghuanetPermission(name = "查询所有")
    public AjaxResult getAll(){
        try {
            List<com.study.cms.auth.domain.Menu> roles = menuService.selectAll();
            return AjaxResult.me().setData(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }

    @PostMapping
    @ApiOperation(value = "分页查询")
    @RonghuanetPermission(name = "分页查询")
    public AjaxResult PageList(@RequestBody MenuQuery menuQuery){
        System.out.println(menuQuery);
        try {
            PageList<Menu> menuPageList = menuService.pageList(menuQuery);
            return AjaxResult.me().setData(menuPageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

    @PatchMapping
    @ApiOperation(value = "批量删除")
    @RonghuanetPermission(name = "批量删除")
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        System.out.println(ids);
        try {
            menuService.patchremove(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败");
        }
    }


    @GetMapping("/getrolemenu/{id}")
    public AjaxResult getmenubyid(@PathVariable("id")Long id){
        try {
            List<Long> ids = menuService.geymenuid(id);
            return AjaxResult.me().setData(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }

    @GetMapping("/tree/{id}")
    public AjaxResult getmenuTree(@PathVariable("id")Long id){
        try {
           List<com.study.cms.auth.domain.Menu> menu = menuService.gettree(id);
            System.out.println("=========");
            System.out.println(menu);
           return AjaxResult.me().setData(menu);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("菜单树查询失败"+e.getMessage());
        }

    }

}
