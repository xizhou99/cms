package com.study.cms.auth.controller;


import com.study.cms.auth.Dto.MenuDTO;
import com.study.cms.auth.Dto.RolePermissionDTO;
import com.study.cms.auth.annotation.RonghuanetPermission;
import com.study.cms.auth.domain.Menu;
import com.study.cms.auth.domain.Role;
import com.study.cms.auth.service.RoleService;
import com.study.cms.basic.query.RoleQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.org.utils.AjaxResult;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/role")
@RonghuanetPermission(name = "角色管理")
public class RoleController {

    @Autowired
    private RoleService roleService;

    //  新增/修改
    @PutMapping
    @RonghuanetPermission(name = "新增/修改")
    public AjaxResult addorupdate(@RequestBody com.study.cms.auth.domain.Role role){
        try {
            if(Objects.isNull(role.getId())){
                System.out.println("-----insert------");
                System.out.println(role);
                //@TODO 调用departmentService做新增操作
                roleService.insert(role);
            }else{
                //@TODO 调用departmentService做修改操作
                System.out.println("-----update------");
                System.out.println(role);
                roleService.update(role);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败!");
        }
        return AjaxResult.me();
    }


    @DeleteMapping("/{id}")
    @RonghuanetPermission(name = "删除")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            roleService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!");
        }
        return AjaxResult.me();
    }

    @GetMapping("/{id}")
    @RonghuanetPermission(name = "通过id查询")
    public AjaxResult getbyid(@PathVariable("id")Long id){
        try {
            // @TODO 调用departmentService的查询方法
            com.study.cms.auth.domain.Role role = roleService.selectByid(id);
            return AjaxResult.me().setData(role);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }


    @GetMapping
    @ApiOperation(value = "查询所有" )
    @RonghuanetPermission(name = "查询所有")
    public AjaxResult getAll(){
        try {
            List<com.study.cms.auth.domain.Role> roles = roleService.selectAll();
            return AjaxResult.me().setData(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败!");
        }
    }

    @PostMapping
    @ApiOperation(value = "分页查询")
    @RonghuanetPermission(name = "分页查询")
    public AjaxResult PageList(@RequestBody RoleQuery roleQuery){
        System.out.println(roleQuery);
        try {
            PageList<Role> rolePageList = roleService.pageList(roleQuery);
            return AjaxResult.me().setData(rolePageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

    @PatchMapping
    @ApiOperation(value = "批量删除")
    @RonghuanetPermission(name = "批量删除")
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        System.out.println(ids);
        try {
            roleService.patchremove(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败");
        }
    }

    /**
     * 设置角色的权限
     * 如果前端传递的参数,后端没有与之对应的domain来接收,那么我们可以用Map接收,或者重新定义个DTO对象
     * @param dto
     * @return
     */
    @PostMapping("/permission")
    @RonghuanetPermission(name = "角色权限")
    public AjaxResult setRolePermisison(@RequestBody RolePermissionDTO dto){
        try {
            roleService.setRolePermisison(dto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }

    @PostMapping("/menu")
    @RonghuanetPermission(name = "角色菜单")
    public AjaxResult setRoleMenu(@RequestBody MenuDTO dto){
        try {
            roleService.setRoleMenu(dto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }

    @GetMapping("/menutree")
    @RonghuanetPermission(name = "菜单树")
    public AjaxResult tree(){
        try {
            List<Menu> list =roleService.gettree();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("菜单树查询失败!"+e.getMessage());
        }
    }

    @GetMapping("/ids")
    public AjaxResult getMenu(){
        try {
            List<Long> ids = roleService.getmenuid();
            return AjaxResult.me().setData(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }


}
