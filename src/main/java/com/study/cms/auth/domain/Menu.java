package com.study.cms.auth.domain;

import lombok.Data;

import java.util.List;

@Data
public class Menu {


            private Long id ;
            private String name;
            private String url;
            private String icon;
            private Long parent_id;
            private Menu parent;
            private List<Menu> children;

}
