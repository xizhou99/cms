package com.study.cms.auth.domain;


import lombok.Data;

@Data
public class Role {
    private Long id;
    private String name;
    private String sn;
}
