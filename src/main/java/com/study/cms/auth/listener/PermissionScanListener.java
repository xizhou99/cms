package com.study.cms.auth.listener;


import com.study.cms.auth.service.PermissionScanService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * ServletContextListener servlet容器(tomcat)一启动完成就要来执行某个事情
 */
@WebListener    // 会将当前类交给spring来管理
public class PermissionScanListener implements ServletContextListener {

    @Autowired
    private PermissionScanService permissionScanService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("------------------开始扫描----------------");
        // 执行权限注解扫描的逻辑
//        permissionScanService.scan();
        System.out.println("-------------------结束扫描---------------");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("容器销毁.................................");
    }
}
