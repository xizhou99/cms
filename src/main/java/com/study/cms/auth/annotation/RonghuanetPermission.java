package com.study.cms.auth.annotation;


import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME) //运行时
@Documented
public @interface RonghuanetPermission {

    String name();//不能为null
    String desc() default ""; //默认值

}
