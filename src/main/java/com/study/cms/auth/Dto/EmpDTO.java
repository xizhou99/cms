package com.study.cms.auth.Dto;

import lombok.Data;

import java.util.List;

@Data
public class EmpDTO {
    private Long empid;
    private List<Long> roleid;
}
