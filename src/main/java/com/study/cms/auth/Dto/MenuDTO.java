package com.study.cms.auth.Dto;

import lombok.Data;

import java.util.List;

@Data
public class MenuDTO {
    private Long roleId;
    private List<Long> Menuid;
}
