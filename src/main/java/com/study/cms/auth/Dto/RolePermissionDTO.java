package com.study.cms.auth.Dto;


import lombok.Data;

import java.util.List;

/**
 * DTO data transfer object:数据传输对象,dto一般都是用于接收后端不能用domain接收的情况
 *      InDTO:用于接收前端的参数
 *      OutDTO:用于后端往前端返回的对象
 * VO: view object 用于后端往前端返回值时,不能用domain返回的情况
 */
@Data
public class RolePermissionDTO {

    private Long roleId;
    private List<String> permissionSns;
}
