package com.study.cms.auth.mapper;


import com.study.cms.auth.domain.Menu;

import com.study.cms.basic.mapper.BaseMapper;

import java.util.List;

public interface MenuMapper  extends BaseMapper<Menu> {



    List<Long> getmenuid(Long id);

    List<Menu> gettree(Long id);
}
