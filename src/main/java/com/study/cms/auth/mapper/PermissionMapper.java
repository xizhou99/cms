package com.study.cms.auth.mapper;


import com.study.cms.auth.domain.Permission;

import com.study.cms.basic.mapper.BaseMapper;

import java.util.List;

public interface PermissionMapper  extends BaseMapper<Permission> {


//    void insert(Permission permission);
    void removeAll();
//    long loadTotal(PermissonQuery query);
//    List<Permission> loadData(PermissonQuery query);

    List<Permission> gettree();

    List<String> getPermissionSns();

    List<String> getSnsbyid(Long id);

    List<String> getsnsbyEmpid(Long id);
}
