package com.study.cms.auth.mapper;

import com.study.cms.auth.Dto.MenuDTO;
import com.study.cms.auth.Dto.RolePermissionDTO;
import com.study.cms.auth.domain.Menu;

import com.study.cms.basic.mapper.BaseMapper;

import java.util.List;

public interface RoleMapper extends BaseMapper<com.study.cms.auth.domain.Role> {
//    List<RoleQuery> selectAll();
//    RoleQuery selectbyid(Serializable id);
//
//    void insert(RoleQuery role);
//    void update(RoleQuery role);
//    void delete(Serializable id);
//
//
//    long loadTotal(RoleQuery query);
//    List<RoleQuery> loadData(RoleQuery query);
//
//    void patchremove(List<Long> list);

    void deleteRolePermissionByRoleId(Long roleId);

    void saveRolePermission(RolePermissionDTO rolePermissionDTO);

    List<Menu> gettree();

    List<Long> getmenuid();

    void deleteMenuByRoleId(Long roleId);

    void saveMenu(MenuDTO dto);
}
