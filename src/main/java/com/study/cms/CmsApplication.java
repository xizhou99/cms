package com.study.cms;

import com.study.cms.auth.interceptor.LoginInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 */
@SpringBootApplication
@MapperScan("com.study.cms.*.mapper")
@ServletComponentScan("com.study.cms.auth.listener")
public class CmsApplication implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class,args);
    }

    @Autowired
    private LoginInterceptor loginInterceptor;

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(loginInterceptor)
//                .addPathPatterns("/**")
//                .excludePathPatterns("/login","/logout");
//
//    }
}
