package com.study.cms.sys.mapper;

import com.study.cms.sys.domain.Config;
import com.study.cms.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xizhou
 * @since 2023-03-13
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
