package com.study.cms.sys.mapper;

import com.study.cms.sys.domain.Dictionary;
import com.study.cms.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xizhou
 * @since 2023-03-13
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}
