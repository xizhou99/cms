package com.study.cms.sys.controller;

import com.study.cms.sys.service.IDictionaryitemService;
import com.study.cms.sys.domain.Dictionaryitem;
import com.study.cms.sys.query.DictionaryitemQuery;
import com.study.cms.basic.utils.PageList;
import com.study.cms.basic.utils.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dictionaryitem")
public class DictionaryitemController {
    @Autowired
    public IDictionaryitemService dictionaryitemService;


    /**
     * 保存和修改公用的
     * @param dictionaryitem  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Dictionaryitem dictionaryitem){
        try {
            if( dictionaryitem.getId()!=null)
                dictionaryitemService.update(dictionaryitem);
            else
                dictionaryitemService.insert(dictionaryitem);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            dictionaryitemService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            Dictionaryitem dictionaryitem = dictionaryitemService.selectByid(id);
            return AjaxResult.me().setData(dictionaryitem);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public AjaxResult list(){

        try {
            List< Dictionaryitem> list = dictionaryitemService.selectAll();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public AjaxResult json(@RequestBody DictionaryitemQuery query)
    {
        try {
            System.out.println("--------数据字典明细------");
            PageList<Dictionaryitem> pageList = dictionaryitemService.pageList(query);
            System.out.println(pageList);
            return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }


    //批量删除
    @PatchMapping
    public AjaxResult patchremove(@RequestBody List<Long> ids)
    {
        try {
           dictionaryitemService.patchremove(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("批量删除失败！"+e.getMessage());
        }
    }
}
