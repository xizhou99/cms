package com.study.cms.sys.service.impl;

import com.study.cms.sys.domain.Dictionary;
import com.study.cms.sys.service.IDictionaryService;
import com.study.cms.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xizhou
 * @since 2023-03-13
 */
@Service
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary> implements IDictionaryService {

}
