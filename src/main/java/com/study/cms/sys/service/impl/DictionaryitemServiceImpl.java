package com.study.cms.sys.service.impl;

import com.study.cms.sys.domain.Dictionaryitem;
import com.study.cms.sys.service.IDictionaryitemService;
import com.study.cms.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xizhou
 * @since 2023-03-13
 */
@Service
public class DictionaryitemServiceImpl extends BaseServiceImpl<Dictionaryitem> implements IDictionaryitemService {

}
