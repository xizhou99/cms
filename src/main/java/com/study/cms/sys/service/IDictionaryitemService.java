package com.study.cms.sys.service;

import com.study.cms.sys.domain.Dictionaryitem;
import com.study.cms.basic.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xizhou
 * @since 2023-03-13
 */
public interface IDictionaryitemService extends BaseService<Dictionaryitem> {

}
